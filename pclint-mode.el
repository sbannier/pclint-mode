;;; pclint-mode -- A major mode to edit PCLint configuration files

;;; Commentary:


;;; Code:

;;(defconst pclint-mode-kwlist
;;  '("e" "esym" "emacro" "estring"))

;;(defconst pclint-mode-kw2 (mapcar (lambda (x) (concat "+" x)) pclint-mode-kwlist))

(defconst pclint-mode-font-lock-keywords-1
  `((,(regexp-opt '("+esym" "-esym" "+emacro" "-emacro" "+d" "+D")
                  ;;'words
                  ;;'symbols
                  t
                  )
     . font-lock-keyword-face)
    ;;("\\s-*\\+[Dd]" . font-lock-keyword-face)
    )
  "TODO.")


(defvar pclint-mode-font-lock-keywords)
(setq pclint-mode-font-lock-keywords
      pclint-mode-font-lock-keywords-1
      ""
      )

(define-derived-mode pclint-mode fundamental-mode "PCLint"
;;  (make-local-variable 'indent-line-function)
  (setq font-lock-defaults
        '((pclint-mode-font-lock-keywords)
          nil
          nil
          ((?+ . "w")
           (?_ . "w")
           (?\( . "()")
           (?\) . ")(")
           (?/ . ". 124") ; C style comment
           (?* . ". 23b") ; C style comment
           (?\n . ">") ; Comment end
           ))))


(add-to-list 'auto-mode-alist '("\\.lnt\\'" . pclint-mode))


(provide 'pclint-mode)
;;; pclint-mode.el ends here
